<?php
function debug($array=[],$exit=0){
    echo '<pre>';
    print_r($array);
    echo '</pre>';
    if($exit){
        exit();
    }
}
function getIP() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
function _mail($data){
    //$dataEmail = ['to'=>'','subject'=>'',$cc=>'','body'=>'','files'=>[],'sender'=>email_sender,'replyTo'=>email_reply_to];
    try{
        \Mail::send([], [], function ($message) use ($data){
            $message->to($data['to'])
                ->subject($data['subject'])
                ->replyTo('noreply@remindme.me')
                ->from ('noreply@remindme.me','remindme.me')
                ->sender('noreply@remindme.me');
            if(isset($data['cc']) && trim(!empty($data['cc']))){
                $message->cc($cc);
            }
            if(isset($data['files']) && !empty($data['files']) && sizeof($data['files']) > 0){
                for( $i=0; $i<count($data['files']); $i++ ){
                    $message->attach($data['files'][$i]); // change i to $i
                }
            }
            $message = $message->setBody($data['body'],'text/html');
        });
    }catch(Exception $e){
        echo $e->getMessage();
        exit;
        return false;
    }
    return ['Operation'=>'True','Message'=>'Email Sent Successfully.'];
}
function drawErrors(){
    $returnStr = '';

    if (\Session::has('errors')){
        $errorsStr = '';
        $errors = \Session::get('errors')->toArray();
        foreach($errors as $error){
            $errorsStr .= $error[0].'<br>';
        }
        $returnStr = '<div class="alert alert-danger errordivMain"><span  onclick="hideErrorDiv()" class="pull-right"  style="color:#933432; font-size: 20px;line-height: 15px; cursor: pointer;" >×</span>'.$errorsStr.'</div>';
    }
    if(\Session::has('success') && !empty(\Session::get('success'))){
        $returnStr = '  <div class="alert alert-success errordivMain"><span  onclick="hideErrorDiv()" class="pull-right" style="color:#2b542c; font-size: 20px;line-height: 15px;cursor: pointer;" >×</span>'.\Session::get('success').'</div>';
    }
    return $returnStr;
}
if(!function_exists('get_headers')){
    function get_headers($url,$format=0){
        $url=parse_url($url);
        $end = "\r\n\r\n";
        $fp = fsockopen($url['host'], (empty($url['port'])?80:$url['port']), $errno, $errstr, 30);
        if ($fp)
        {
            $out  = "GET / HTTP/1.1\r\n";
            $out .= "Host: ".$url['host']."\r\n";
            $out .= "Connection: Close\r\n\r\n";
            $var  = '';
            fwrite($fp, $out);
            while (!feof($fp))
            {
                $var.=fgets($fp, 1280);
                if(strpos($var,$end))
                    break;
            }
            fclose($fp);

            $var=preg_replace("/\r\n\r\n.*\$/",'',$var);
            $var=explode("\r\n",$var);
            if($format)
            {
                foreach($var as $i)
                {
                    if(preg_match('/^([a-zA-Z -]+): +(.*)$/',$i,$parts))
                        $v[$parts[1]]=$parts[2];
                }
                return $v;
            }
            else
                return $var;
        }
    }    
}
function _encode($str){
    $str = base64_encode($str);
    $str = str_replace('=','gcc_ab',$str);
    $str = str_replace('Z','jazznju',$str);
    //$str = gzcompress($str, 9);
    $str = strtr(base64_encode($str), '+/=', '._-');
    return urlencode($str);
}
function _decode($str){
    $str = base64_decode(strtr(urldecode($str), '._-', '+/='));
    //$str = gzuncompress($str);
    $str = str_replace('gcc_ab','=',$str);
    $str = str_replace('jazznju','Z',$str);
    
    $str = base64_decode($str);
    
    return $str;
}
function array_to_csv_download($array) {
    $file_name = 'file-'.time().'.csv';

    $dir_name = public_path().'/csv/'.$file_name;
    $fp = fopen($dir_name, 'w');
    foreach ($array as $fields) {
        fputcsv($fp, $fields);
    }
    fclose($fp);
    return $file_name;
}