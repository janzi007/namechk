<?php

namespace App\Http\Controllers;

use App\Extension, App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use View, Validator, Redirect, Auth, URL, Uuid, Hash;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }
    public function getAllExtensions(){
        $input = Input::all();
        //debug($input,1);
        $extensions = DB::table('extensions');
        if(isset($input['search']) && !empty($input['search'])){
            $extensions = $extensions->where('title','LIKE','%'.trim($input['search']).'%')->orWhere('href','LIKE','%'.trim($input['search']).'%');    
        }
        $extensions = $extensions->paginate('20');
        return View('dashboard.extension.extensions',['extensions'=>$extensions]);
    }
    public function insert(){
        return View('dashboard.extension.addExtension');
    }
    public function insertPost(){
        $extension = Input::all();
        $rules = array(
            'li_class' => 'required|unique:extensions|max:255',
            'href' => 'required',
            //'http_username_url' => 'required',
            'title' => 'required',
        );
        $validation = Validator::make(Input::all(), $rules);
        if ($validation->fails()) {
            return Redirect::back()->withInput()->withErrors($validation->messages());
        } else {
            $extension = new Extension;
            $extension->li_class = Input::get('li_class');
            $extension->href = Input::get('href');
            $extension->http_username_url = Input::get('http_username_url');
            $extension->title = Input::get('title');
            $extension->status = Input::get('status');
            $extension->save();
            return Redirect::route('ExtensionsAll')->with('success','Extension Added Successfully.');
        }
    }
    
    public function destroy($id){
        DB::delete('delete from extensions where id = ?',[$id]);
        return Redirect::route('ExtensionsAll')->with('success','Extension Deleted Successfully.');
    }
    public function edit($id){
        $extension = DB::table('extensions')->where('id','=',$id)->first();
        return view('dashboard.extension.editExtension',compact('extension'));
    }
    public function update(Request $request)
    {
        $extension = Input::all();
        $rules = array(
            'li_class' => 'required|unique:extensions,li_class,'.$request->id,
            'href' => 'required',
//            'http_username_url' => 'required',
            'title' => 'required',
        );
        $validation = Validator::make(Input::all(), $rules);
        if ($validation->fails()) {
            return Redirect::back()->withInput()->withErrors($validation->messages());
        } else {
            $extension = Extension::find($request->id);
            $extension->li_class = Input::get('li_class');
            $extension->href = Input::get('href');
            $extension->http_username_url = Input::get('http_username_url');
            $extension->title = Input::get('title');
            $extension->status = Input::get('status');
            $extension->save();
            return Redirect::route('ExtensionsAll')->with('success','Extension Update Successfully.');
        }
    }
    function logout(){
        Auth::logout();
        return Redirect::to(Route('index'));
    }
    function profile(){
        return View('dashboard.profile');
    }
    function updateProfile(){
        $post = Input::all();
        if(trim($post['type']) == 'profile'){
            $rules = array(
                'name' => 'required',
            );
            $validation = Validator::make($post, $rules);
            if ($validation->fails()) {
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }
            $name = $post['name'];
            User::whereId(Auth::user()->id)->update(['name'=>$name]);
            return Redirect::back()->with('success','Profile Update Successfully.');
        }
        if(trim($post['type']) == 'password'){
            $rules = array(
                'old_passsword' => 'required',
                'new_passsword' => 'required',
                'confirm_passsword' => 'required',
            );
            $validation = Validator::make($post, $rules);
            if ($validation->fails()) {
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }
            if($post['new_passsword'] != $post['confirm_passsword']){
                return Redirect::back()->withInput()->withErrors(['Old password and new password must be same.']);
            }
            if (!(Hash::check($post['old_passsword'], Auth::user()->password))) {
                return redirect()->back()->withErrors(["Your current password does not matches with the password you provided. Please try again."]);
            }
            $password = Hash::make($post['new_passsword']);
            User::whereId(Auth::user()->id)->update(['password'=>$password]);
            return Redirect::back()->with('success','Password Update Successfully.');
        }
    }
    public function ViewClientProfile(){
        $user = User::all();
        return View('dashboard.extension.user_profile',['users'=>$user]);
    }
    public function userdestroy($id){
        DB::delete('delete from users where id = ?',[$id]);
        return Redirect::route('ViewClientProfile')->with('success','Extension Deleted Successfully.');
    }
    public function useredit($id)
    {
        $user = DB::table('users')->where('id', '=', $id)->first();
        return view('dashboard.extension.edit_profile', compact('user'));
    }
    function updateUserProfile($id){
        $user = Input::all();
        if(trim($user['type']) == 'profile'){
            $rules = array(
                'name' => 'required',
                'email' => 'required',
                'role' => 'required',
            );
            $validation = Validator::make($user, $rules);
            if ($validation->fails()) {
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }
            $name = $user['name'];
            $email= $user['email'];
            $role = $user['role'];
            User::whereId($id)->update(['name'=>$name,'email'=>$email,'role'=>$role]);
            return Redirect::back()->with('success','Profile Update Successfully.');
        }
        if(trim($user['type']) == 'password'){
            $rules = array(
                'new_passsword' => 'required',
                'confirm_passsword' => 'required',
            );
            $validation = Validator::make($user, $rules);
            if ($validation->fails()) {
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }
            if($user['new_passsword'] != $user['confirm_passsword']){
                return Redirect::back()->withInput()->withErrors(['Old password and new password must be same.']);
            }
            $password = Hash::make($user['new_passsword']);
            User::whereId($id)->update(['password'=>$password]);
            return Redirect::back()->with('success','Password Update Successfully.');
        }
    }

}
