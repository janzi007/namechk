<?php

namespace App\Http\Controllers;

use App\Extension;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Providers\NoCaptchaServiceProvider;

use View, Validator, Redirect, Auth, URL;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    var $paginateLimit;
    public function __construct()
    {
        $this->paginateLimit = 55;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Input::all();
        $searchStr = '';
        if(isset($input['search']) && !empty(trim($input['search']))){
            $messages = [
                'g-recaptcha-response.required' => 'Please validate Captcha before search.',
            ];
            $validate = Validator::make(Input::all(), [
                'g-recaptcha-response' => 'required|captcha'
            ],$messages);
            if($validate->fails()){
                return Redirect::route('index')->withInput()->withErrors($validate->messages());
            }
            $searchStr = trim($input['search']);
        }
        $extensions = DB::table('extensions')->where('status','=','Active')->orderBy('id','asc')->paginate($this->paginateLimit);
        return view('index',['extensions'=>$extensions,'searchStr'=>$searchStr]);
    }
    public function loadMoreExtension(){
        $input = Input::all();

        $validate = Validator::make($input, [
            'g-recaptcha-response' => 'required|captcha'
        ]);
        if($validate->fails()){
            return ['success'=>'false','message'=>'Please validate captcha before show more data.'];
        }
        $searchStr = '';
        if(isset($input['search']) && !empty(trim($input['search']))){
            $searchStr = trim($input['search']);
        }
        $extensions = DB::table('extensions')->where('status','=','Active')->orderBy('id','asc')->paginate($this->paginateLimit);
        $html = View::make('extensions',['extensions'=>$extensions,'searchStr'=>$searchStr])->render();
        return ['success'=>'true','message'=>$html];
    }
    function usernameExists(){
        $post = Input::all();
        
        $response = [];
        $response['key'] = $post['id'];
        $response['title'] = $post['title'];

        $id         = _decode($post['id']);
        $user_name  = trim($post['uname']);
        $http_url   = _decode($post['http']);

        $isHaveInBank = DB::table('not_exists_data_bank')->where('extension_id','=',$id)->where('username','=',$user_name)->limit(1)->count();
        if($isHaveInBank > 0 || empty($http_url)){
            $response['available'] = 'false';
        }else{
            $http_url_check = str_replace('####',$user_name,$http_url);
            $headers = get_headers($http_url_check, 1);
            if(isset($headers['0'])){
                $statusCode = explode(' ', $headers['0']);
                $statusCode = $statusCode[1];
                if(trim($statusCode) == 200){
                    $response['available'] = 'false';
                    DB::table('not_exists_data_bank')->insert(['extension_id'=>$id,'username'=>$user_name]);
                }else{
                    $response['available'] = 'true';
                }
            }else{
                $response['available'] = 'true';
            }    
        }
        return $response;
    }
    function generateCsv(){
        $post = Input::all();
        $validate = Validator::make(Input::all(), [
            'g-recaptcha-response' => 'required|captcha'
        ]);
        if($validate->fails()){
            return ['success'=>'false','message'=>'Please validate captcha before download the CSV.'];
        }
        $data = json_decode($post['data'],1);
        $dataCsv = [];
        $dataCsv[] = ['Extension','Status'];
        foreach ($data as $key => $value) {
            if(trim($value['status']) == 'false'){
                $status = 'Unavailable';    
            }else{
                $status = 'Available';
            }            
            $dataCsv[] = [$value['title'],$status];    
        }
        $generatedCSVURl = array_to_csv_download($dataCsv);

        return ['success'=>'true','message'=>url('csv/'.$generatedCSVURl)];
    }
}