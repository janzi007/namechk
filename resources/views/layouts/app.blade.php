<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0' name='viewport'/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Repoogle</title>
    <meta name="author" content="Repoogle">
    
    <link rel="stylesheet" href="{{url('web/css/style.css')}}">
    <link rel="stylesheet" href="{{url('web/css/icons.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/sweetalert.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|Roboto:400,400i,500,500i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.11/css/all.css" integrity="sha384-p2jx59pefphTFIpeqCcISO9MdVfIm4pNnsL08A6v5vaQc4owkQqxMV8kg4Yvhaw/" crossorigin="anonymous">
    <link rel="icon" href="{{url('favicon.ico')}}" type="image/x-icon">
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
    
</head>
<body>
    <!-- Site Header-->
      <header>
         <div class="wrap">
         <!-- Site Logo -->
            <div class="logo">
               <h1 class="site-title" itemprop="headline"><a href="{{route('index')}}">Repoogle</a></h1>
               <span class="logo-description">Protect your Reputation</span>
            </div>
            <!-- Site Navigation-->
            <div id="navigation" class="menu">
               <button onclick="menuFunction()" class="dropbtn"><i class="fas fa-bars"></i></button>
               <ul class="dropdown-content">
                  @if(Auth::check())
                  <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                  <li><a href="{{route('userlogout')}}">Logout</a></li>
                  @else
                  <li><a href="{{route('login')}}">Login</a></li>
                  <li><a href="{{route('register')}}">Register</a></li>
                  @endif
                  
                  <li><a href="#">Contact Us</a></li>
               </ul>
            </div>
         </div>
      </header>
    @yield('content')
    <!-- Site Footer -->
      <footer class="main-footer">
         <div class="wrap">
            <div class="left">
               <p>Copyright © {{date('Y')}} Repoogle.com</p>
            </div>
            <div class="right">
               <ul>
                  <li><a href="#">About</a></li>
                  <li><a href="#">API</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Terms & Conditions</a></li>
                  <li><a href="#">Feedback</a></li>
               </ul>
            </div>
         </div>
      </footer>
<script src="{{url('web/js/jquery.js')}}"></script>
<script src="{{asset('web/js/sweetalert.min.js')}}"></script>
<script src="{{url('web/js/custom.js')}}"></script>



    @yield('javascript')
    <script>
        $(document).ready(function(){
            @if (\Session::get('success') && \Session::get('success') != '')
swal('Success!','{!!\Session::get('success')!!}','success');
            @endif
            @if($errors && count($errors->all()) > 0)
                @php $erros = implode(' <br> ',$errors->all()); @endphp
swal({ html:true, title:'Error',type: 'error', text:'{!!$erros!!}'});
            @endif
        })
    </script>
</body>
</html>
