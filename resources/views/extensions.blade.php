@foreach($extensions as $extension)
<li class="{!!$extension->li_class!!}" id="{{_encode($extension->id)}}" attr-href="{{_encode($extension->http_username_url)}}" data-title="{{$extension->title}}">
   <a href="#">
	<span class="logo">
		<span class="icon"></span>
	</span>
   <span id="res_{{_encode($extension->id)}}" class="{{isset($searchStr) && !empty($searchStr) ? 'result' : ''}}"></span>
   <span class="username-title">{!!$extension->title!!}</span>
   </a>
</li>
@endforeach