@extends('layouts.app')
@section('content')
 <!-- Main Container-->
<div class="main-container">
   <div class="wrap">
   <!-- Search Bar -->
      <div class="search-bar">
         {{ Form::open(['url' => '','method'=>'GET','class'=>'search','autocomplete'=>'off']) }}
         <div class="search-form">   
            {{  Form::text('search', $searchStr, ['class'=>'form-control','placeholder'=>'Find an Avaliable username, Search Here...','id'=>'unamesearch','required'=>'true']) }}
            <button style="cursor:pointer" type="submit"><i class="fa fa-search"></i></button>
         </div>
         <div class="captcha" style="display:block">{!! Form::captcha() !!}</div>
         {{ Form::close() }}

      </div>
      <!-- Username Logo Section-->
      <section id="userlogo" class="iconclass">
         <div class="username-logo">
            <h2>Usernames</h2>
            <ul id="extensions_main">
               @include('extensions')
            </ul>
         </div>
         {{ Form::open(['url' => '','method'=>'POST','autocomplete'=>'off','id'=>'show_more_form']) }}
         <div class="button-container">
            <div class="captcha" style="margin-left:38%;">{!! Form::captcha() !!}</div>
            <a onclick="loadContent(this.id)" id="load_more_btn" class="load-more">Show More <i style="display:none;" id="loading_btn_spin" class="fa fa-spinner fa-spin"></i></a>
         </div>
         {{ Form::close() }}
         {{ Form::open(['url' => '','method'=>'POST','autocomplete'=>'off','id'=>'generateCsvForm']) }}
         <div class="export-report">
            <h2>Export this Page</h2>
            <a class="export-page" id="get_csv" onclick="getCsv(this.id)">Export this Page as CSV</a>
            <div class="captcha">{!! Form::captcha() !!}</div>
         </div>
         {{ Form::close() }}
      </section>
      <!-- Register Bar -->
      <div class="register-bar">
         <h3>Want us to Register your Usernames?</h3>
         <span class="button"><a href="#">Get Started</a></span>
      </div>
   </div>
</div>
<!-- Substitute Container -->
<div class="sub-container">
   <div class="wrap">
   <!-- Main Content -->
      <main class="main-content">
         <article>
            <h2>What is Repoogle</h2>
            <p>
               Use Repoogle to see if your desired username or vanity url is still available at dozens of popular Social Networking and Social Bookmarking websites. Promote your brand consistently by registering a username that is still available on the majority of the most popular sites. Find the best username with Namechk.
            </p>
            <p>
               Securing your brand is important. It provides consistency to your users, and allows your business to be seen and recognized in more places. Namechk has partnered with some top companies to help you ensure your brand is consistent and easy to find across the internet.
            </p>
            <p>
               <span class="button"><a href="#">Read More About Us</a></span>
            </p>
            <div class="ads-300">
               <img src="{{url('web/images/ad-300.png')}}"/>
            </div>
         </article>
      </main>
      <!-- Sidebar -->
      <aside class="right-sidebar">
         <button class="sidebar-button"><i class="fas fa-donate"></i> Donate Us</button>
         <div class="widget links">
            <h2>Help spread the word</h2>
            <ul>
               <li><a href="#">- Share on Facebook</a></li>
               <li><a href="#">- Share on Twitter</a></li>
            </ul>
         </div>
         <div class="widget register-profile">
            <i class="fas fa-user-plus"></i>
            <h3>We can Register <br/> your all Profiles</h3>
            <a href="#">Click Here</a>
         </div>
      </aside>
   </div>
</div>
@section('javascript')
<script type="text/javascript">
@if(!empty($searchStr))
var canRun = true;
var searchStr = "{{$searchStr}}";
@else
var searchStr = '';
var canRun = false;
@endif
$(document).ready(function(){
   _runApp();
});
</script>

@endsection
@endsection