<header class="main-header">
    <!-- Logo -->
    <a href="{{url('/')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>R</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Repoogle</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <!-- Notifications: style can be found in dropdown.less -->
                <!-- Tasks: style can be found in dropdown.less -->
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(!empty(Auth::user()->image))
                            <img src="{{url('user_images/'.Auth::user()->image)}}" class="user-image" alt="User Image">
                        @else
                        <img src="{{url('dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                        @endif
                        <span class="hidden-xs">{{Auth::user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if(!empty(Auth::user()->image))
                                <img src="{{url('user_images/'.Auth::user()->image)}}" class="img-circle" alt="User Image">
                            @else
                                <img src="{{url('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                            @endif
                            <p>
                                {{Auth::user()->name}}
                                <small>Member since {{date('M, d Y',strtotime(Auth::user()->created_at))}}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route('profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->

            </ul>
        </div>
    </nav>
</header>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
