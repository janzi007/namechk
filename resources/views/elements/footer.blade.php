<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; {{date('Y')}} .</strong> All rights reserved. Made by <a target="_blank" href="http://crewsofts.com">Crewsofts</a>
    .
</footer>
<div class="modal fade" id="modal_ajax_load">
  <div class="modal-dialog" style="width:90%">
    	<div class="modal-content" id="modal_ajax_load_content">
  		</div>  		
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->