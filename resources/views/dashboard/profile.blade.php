@extends('layouts.dashboard')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Profile</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}">Home</a></li>
        <li class="active">Profile</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(['url' => route('updateProfile'),'method'=>'POST','autocomplete'=>'off']) }}            {{  Form::hidden('type', 'profile') }}
              <div class="box-body">
                <div class="form-group">
                    {{ Form::label('Name') }}
                    {{  Form::text('name', Auth::user()->name, ['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('Email') }}
                    {{  Form::text('email', Auth::user()->email, ['class'=>'form-control','readonly']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('Role') }}
                    {{  Form::text('role', Auth::user()->role, ['class'=>'form-control','readonly']) }}
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                {!! Form::submit('Update',['class'=>'btn btn-primary']) !!}
              </div>
            {{ Form::close() }}
          </div>
          <!-- /.box -->
        </div>
        <!-- Right column -->

        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(['url' => route('updateProfile'),'method'=>'POST','autocomplete'=>'off']) }}
            {{  Form::hidden('type', 'password') }}
              <div class="box-body">
                <div class="form-group">
                    {{ Form::label('Old password') }}
                    {{  Form::password('old_passsword', ['class'=>'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('New password') }}
                    {{  Form::password('new_passsword', ['class'=>'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('Confirm password') }}
                    {{  Form::password('confirm_passsword', ['class'=>'form-control']) }}
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                {!! Form::submit('Update Password',['class'=>'btn btn-primary']) !!}
              </div>
            {{ Form::close() }}
          </div>
          <!-- /.box -->
        </div>
      </div>
</section>
@endsection