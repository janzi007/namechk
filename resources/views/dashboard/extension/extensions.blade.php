@extends('layouts.dashboard')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Extensions</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}">Home</a></li>
        <li class="active">Extensions</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Extension</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                
                {{ Form::open(['url' => route('ExtensionsAll'),'method'=>'GET','class'=>'search','autocomplete'=>'off']) }}
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Extension Name</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{  Form::text('search', Input::get('search',''), ['class'=>'form-control','placeholder'=>'Find an Avaliable extension, Search Here...']) }}
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info pull-right">Search <i class="fa fa-plus" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                {{ Form::close() }}
            <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>

        </div>
    </div>    
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Extensions Listing</h3>
              <a href="{{route('addExtension')}}" class="btn btn-primary btn-success pull-right">Add Extension</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>List Class</th>
                        <th>Href</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($extensions)>0)
                    @foreach($extensions as $extension)
                        <tr>
                            <td>{{$extension->id}}</td>
                            <td>{{$extension->li_class}}</td>
                            <td>{{$extension->href}}</td>
                            <td>{{$extension->title}}</td>
                            <td>
                                @if($extension->status == 'Active')
                                <span class="label label-success">Active</span>
                                @else
                                <span class="label label-warning">In Active</span>
                                @endif    
                            </td>
                            <td><a href="{{route('edit',$extension->id)}}">Edit</a></td>
                            <td><a href = {{route('delete',$extension->id)}}' onclick="return window.confirm('Are you sure you want to delete this record?')">Delete</a></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="8" align="center">
                            {{ $extensions->appends(Input::all())->links() }}
                        </td>
                    </tr>
                    @endif
                </tbody>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
</section>
@endsection