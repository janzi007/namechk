@extends('layouts.dashboard')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Edit Extension</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}">Home</a></li>
        <li class="active">Edit Extension</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
             {{ Form::open(['url' => route('update',$extension->id),'method'=>'POST']) }}
              <div class="box-body">
                <div class="form-group">
                    {{ Form::label('List Class') }}
                    {{  Form::text('li_class', $extension->li_class, ['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('Href') }}
                    {{  Form::text('href',$extension->href, ['class'=>'form-control']) }}
                </div>

               <div class="form-group">
                    {{ Form::label('http Username Url (Please use #### in place of username)') }}
                    {{  Form::text('http_username_url', $extension->http_username_url, ['class'=>'form-control','placeholder'=>'www.blabla.com/####/']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('Title') }}
                    {{  Form::text('title',$extension->title, ['class'=>'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('Status') }}
                    {{  Form::select('status', ['Active'=>'Active','In-Active'=>'In-Active'],$extension->status,['class'=>'form-control']) }}
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                {!! Form::submit('Update',['class'=>'btn btn-primary']) !!}
              </div>
            {{ Form::close() }}
          </div>
          <!-- /.box -->
        </div>
      </div>
</section>
@endsection