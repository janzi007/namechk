<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {
	
	Route::get('/', 'HomeController@index')->name('index');
	Route::get('/load-more-extension', 'HomeController@loadMoreExtension')->name('loadMoreExtension');

	Route::post('/username-exists', 'HomeController@usernameExists')->name('usernameExists');

	Route::get('/logout', 'DashboardController@logout')->name('userlogout');
	Route::post('/generate-csv', 'HomeController@generateCsv')->name('generateCsv');
	
	Route::group(['middleware' => ['auth'],'prefix' => 'backoffice'], function () {
		
		Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

		Route::get('extensions', 'DashboardController@getAllExtensions')->name('ExtensionsAll');
		Route::get('extension/add', 'DashboardController@insert')->name('addExtension');
		Route::post('extension/add/post', 'DashboardController@insertPost')->name('insertPost');
		
		
		Route::get('extension/delete/{id}', 'DashboardController@destroy')->name('delete');
		Route::get('extension/edit/{id}', 'DashboardController@edit')->name('edit');
		Route::post('extension/update/{id}', 'DashboardController@update')->name('update');
		Route::get('profile', 'DashboardController@profile')->name('profile');
		Route::post('profile-update', 'DashboardController@updateProfile')->name('updateProfile');

        Route::get('viewclientprofile', 'DashboardController@ViewClientProfile')->name('ViewClientProfile');
        Route::get('user/delete/{id}', 'DashboardController@userdestroy')->name('userdelete');
        Route::get('user/edit/{id}', 'DashboardController@useredit')->name('useredit');
        Route::post('user/update/{id}', 'DashboardController@updateUserProfile')->name('updateUserProfile');
	});

});
Auth::routes();