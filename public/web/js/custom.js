$(document).ready(function(e) { 
   // Close the dropdown if the user clicks outside of it
   window.onclick = function(event) {
     if (!event.target.matches('.dropbtn')) {
   
       var dropdowns = document.getElementsByClassName("dropdown-content");
       var i;
       for (i = 0; i < dropdowns.length; i++) {
         var openDropdown = dropdowns[i];
         if (openDropdown.classList.contains('show')) {
           openDropdown.classList.remove('show');
         }
       }
     }
   }
});
function menuFunction() {
   document.getElementById("navigation").classList.toggle("show");
}
var current_page = '2';
function loadContent(_id){
  if($('#'+_id).is('[disabled=disabled]')){
    return false;
  } else {
    $('#loading_btn_spin').show();
    $('#'+_id).attr('disabled','disabled');
    $.ajax({
      type: "GET",
      url: "load-more-extension?page="+current_page+'&search='+searchStr,
      data: $('#show_more_form').serialize(),
      cache: false,
      success: function(result){
        $('#'+_id).removeAttr('disabled');
        $('#loading_btn_spin').hide();
        resetCaptcha();
        try {
          result = JSON.parse(result);
        } catch (e) {}
        if(result.success == 'true'){
          if($.trim(result.message) == ''){
            $('#load_more_btn').remove();
          }else{
            $('#extensions_main').append(result.message);
            _runApp();
            current_page++;
          }  
        }else{
            swal('Error!',result.message,'error');
        }
      },
      error: function (request, status, error) {
        resetCaptcha();
        $('#'+_id).removeAttr('disabled');
        $('#loading_btn_spin').hide();
      }
    });
  }
}
function _runApp(){
  if(canRun == false){
    return false;
  }
  $("#extensions_main li").each(function(n) {
      if(this.id != ''){
        var _http_url = $(this).attr('attr-href');
        var _title = $(this).attr('data-title');
        
        if($(this).is('[disabled=disabled]')){
          //Do nothing
        } else {  
          getUsernameExists(this.id,_http_url,_title);
        }
      }
  });
}
var jsonResponseObj = [];
function getUsernameExists(_id,_http_url,_title){
  var dataStr = "id="+_id+'&uname='+encodeURIComponent($('#unamesearch').val())+'&http='+_http_url+'&title='+_title;
  $.ajax({
      type: "POST",
      url: "username-exists",
      data: dataStr,
      cache: false,
      success: function(result){
        try {
          result = JSON.parse(result);
        } catch (e) {}
        
        var isAvailable = result.available;
        var mainId = result.key;
        var isKey = '#res_'+mainId;
        $(isKey).addClass(isAvailable);
        $('#'+mainId).attr('disabled','disabled');

        itemInsert = {}
        itemInsert ["title"] = result.title;
        itemInsert ["status"] = isAvailable;
        jsonResponseObj.push(itemInsert);
      },
      error: function (request, status, error) {
        $('res_'+_id).addClass('false');
      }
    });
}
function getCsv(_id){
  if($('#'+_id).is('[disabled=disabled]')){
    return false;
  } else {
    swal('','System is generating CSV, Please wait.','warning');
    $('#'+_id).attr('disabled','disabled');
    $.ajax({
      type: "POST",
      url: "generate-csv",
      data: $('#generateCsvForm').serialize()+'&data='+JSON.stringify(jsonResponseObj),
      cache: false,
      success: function(result){
        resetCaptcha();
        $('#'+_id).removeAttr('disabled');
        try {
          result = JSON.parse(result);
        } catch (e) {}
        if(result.success == 'false'){
          swal('Error!',result.message,'error');
        }else{
          swal.close();
          window.location.href = result.message;
        }
      },
      error: function (request, status, error) {
        resetCaptcha();
        $('#'+_id).removeAttr('disabled');
        swal('Error!','CSV file is not generate, Please try again.','error');
      }
    });
  }
}
function resetCaptcha(){
   grecaptcha.reset(0);
   grecaptcha.reset(1);
   grecaptcha.reset(2);
}